import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}&{state}&{1}"
    headers = {"Authorization": PEXELS_API_KEY}
    # params = {
    #     "per_page": 1,
    #     "query": city + "&" + state,
    # }
    response = requests.get(url, headers=headers)
    link = json.loads(response.content)
    try:
        return {"pic_url": link["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"pic_url": None}


def get_weather(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    geo_param = {
        "q": {city,state},
        "appid": OPEN_WEATHER_API_KEY
    }
    response = requests.get(url, params=geo_param)
    link = json.loads(response.content)
    lat = link[0]["lat"]
    lon = link[0]["lon"]

    url2 = "https://api.openweathermap.org/data/2.5/weather"
    weather_param = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY
    }
    response2 = requests.get(url2, params=weather_param)
    link2 = json.loads(response2.content)
    try:
        return {
            "temp": link2["main"]["temp"],
            "weather_d": link2["weather"][0]["description"]
        }
    except (KeyError, IndexError):
        return {
            "temp": None,
            "weather_d": None,
        }
